const {ccclass, property} = cc._decorator;

@ccclass
export default class Duck extends cc.Component {

    private leftDown: boolean = false; 
    private rightDown: boolean = false; 
    private upDown: boolean = false; 
    private xSpeed: number = 0;
    private mainCamera: cc.Node = null;
    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

    }

    start () {
        this.mainCamera = cc.find("Canvas/Main Camera");
        this.xSpeed = 200;
    }

    update(dt) {
        this.moveControl(dt);

    }
    moveControl (dt) {
        if(this.leftDown) {
            this.xSpeed = -300;
            
            if(this.node.scaleX > 0)
                this.node.scaleX *= -1;
        }
        else if(this.rightDown) {
            this.xSpeed = 300;

            if(this.node.scaleX < 0)
                this.node.scaleX *= -1;
            
        }
        
        if(this.upDown) {
            this.jump();
        }
        this.node.x += this.xSpeed * dt;
        this.mainCamera.x = this.node.x;
    }
    jump() {
        
        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 300);
    }
    onKeyDown (event) {
        // set a flag when key pressed
        switch(event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = true;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = true;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
                console.log("??");
                this.upDown = true;
                break;
        }
    }
    onKeyUp (event) {
        // unset a flag when key released
        switch(event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = false;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = false;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
                this.upDown = false;
                break;
        }
        this.xSpeed = 0;
    }
}
