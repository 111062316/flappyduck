"use strict";
cc._RF.push(module, '24b2426FgtLTr1Kqx6MbQqS', 'Duck');
// Scripts/Duck.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Duck = /** @class */ (function (_super) {
    __extends(Duck, _super);
    function Duck() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.leftDown = false;
        _this.rightDown = false;
        _this.upDown = false;
        _this.xSpeed = 0;
        _this.mainCamera = null;
        return _this;
    }
    Duck.prototype.onLoad = function () {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    };
    Duck.prototype.start = function () {
        this.mainCamera = cc.find("Canvas/Main Camera");
        this.xSpeed = 200;
    };
    Duck.prototype.update = function (dt) {
        this.moveControl(dt);
    };
    Duck.prototype.moveControl = function (dt) {
        if (this.leftDown) {
            this.xSpeed = -300;
            if (this.node.scaleX > 0)
                this.node.scaleX *= -1;
        }
        else if (this.rightDown) {
            this.xSpeed = 300;
            if (this.node.scaleX < 0)
                this.node.scaleX *= -1;
        }
        if (this.upDown) {
            this.jump();
        }
        this.node.x += this.xSpeed * dt;
        this.mainCamera.x = this.node.x;
    };
    Duck.prototype.jump = function () {
        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 300);
    };
    Duck.prototype.onKeyDown = function (event) {
        // set a flag when key pressed
        switch (event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = true;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = true;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
                console.log("??");
                this.upDown = true;
                break;
        }
    };
    Duck.prototype.onKeyUp = function (event) {
        // unset a flag when key released
        switch (event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = false;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = false;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
                this.upDown = false;
                break;
        }
        this.xSpeed = 0;
    };
    Duck = __decorate([
        ccclass
    ], Duck);
    return Duck;
}(cc.Component));
exports.default = Duck;

cc._RF.pop();